from django.conf import settings

from surface_graphene.tastypie.graphql_resource import GraphQLResource
from surface_graphene.tastypie.schema_factory import SchemaFactory

__all__ = ['schema', 'GraphQLResource']


# Lazy evaluation so apps can load before use
def schema():
    return SchemaFactory.create_from_api(settings.TASTYPIE_API_PATH)
